[[_TOC_]]

# About Product Design

Product Design of Karmapedia has been attempted by various directions. Refer to the sibling repositories to this `About Product Design` repository to learn about those attempts.

Here are some of those attempts in the chronoligical order
1. [Roadmap.sh Task Analysis](https://gitlab.com/karmapedia/product-design/roadmap.sh-task-analysis) - A Visual way to discover tools/people/content useful for your "Career Roadmap"
2. [Skillinventory using Airtable]() - @maneeshms to fill here
3. [Hackr.io Clone](https://gitlab.com/karmapedia/product-design/hackr.io-clone) - A clone of Hackr.io. In addition to Content Discovery like Hackr.io, this clone also helps you discover People and Tools - unlike Hackr.io. No Career Roadmap.
4. [Course Recommendation using WWW Crawler](https://docs.google.com/document/d/1EVNYajduJ-pfSFV9aVp-pWe6ELFI2FlTFWXh898CaQ8/edit)
5. [Gitlab Task Analysis](https://gitlab.com/karmapedia/requirements/gitlab-task-analysis) - At Smarter.Codes we are currently use Gitlab to perform all the features that Karmapedia would provide. 
6. [User's Browsing History, to Skillinventory] - Filling your Skill Inventory by monitoring your internet activity. @tushar12123 to fill here. 
7. [Karmapedia and Objective.Earth] - see [www.Objective.Earth](www.Objective.Earth), its work in progress. 
8. [Self Evaluation on your Career Roadmap] - @bahana.s and @Harsha_Peddinti
 to fill here. 
9. [User's WhatsApp Conversations, to Skillinventory] - Filling you Skill Inventory by observing messages you in Social Networking Platforms or on WhatsApp. @Chandan.codes to fill here
10. A Search Engine, but with a "Table of Contents" - so that [search needs of Garrett Dimon](https://garrettdimon.com/2018/starting-a-non-profit/) can be fulfilled

# WHAT is Karmapedia
Karmapedia aspires to be 
> The PLACE<sup>1</sup> where the WORLD<sup>2</sup> comes to WORK<sup>3</sup>

**1. Place.**  
It would be mix of
* LinkedIn (social media)
* Wikibooks (wikipedia, but for books)
* StackOverflow (where people can upvote and decide stuff)

**2. World.**  
Much like Wikipedia, Karmapedia's endeavour is to involve the mankind in crowdsourcing
* what HAS BEEN DONE (by measuring)
* what NEEDS TO BE DONE (by setting goals)
* HOW needs to be done (by setting guidelines)

**3. Work.**  
Work not just to 'earn money' (with Digital Marketing, or Software Development). But especially to work on 'callings' like 
* Building the world. Like [Sustainability Development Goals](https://sdgs.un.org/goals). 
* Hobbies. Like Home Gardening. 
* and anything that pays you (in KPIs) other than the money KPI. 

# What Karmapedia may evolve into
* A Search Engine for Professional Users. Where people discover content (for learning), people. tools for their work.
* A Social Network for Professional Users. Where users build their profiles using micro-interactions (their browsing history, their social footprint.talking to our chatbot)
* OKR applied on planet scale. Open for all - than applied just on a specific organization. 
* A user manual for mankind. A manual for Earth-lings. A manual for Mars-lings. A manual for decentralized mankind (living in different planets). Will [help SpaceX in Mars settlement](https://www.news18.com/news/buzz/elon-musks-spacex-will-make-its-own-laws-on-mars-not-follow-universal-ones-3030056.html).
* A competitor to CMMI Standard. And ISO standards. Because Karmapedia insists on 'See how others on internet have done it', our standards would be more 'hands on practical' than 'thereoratical'.

# WHY we are uniquely positioned to build Karmapedia
See [Why Smarter.Codes should build Karmapedia](https://gitlab.com/karmapedia/requirements-and-architecture/-/blob/master/why-Karmapedia.md#why-smartercodes-should-build-karmapedia)
